<?php

namespace App\Http\Controllers;

use App\Article;
use App\Http\Requests\article\StoreArticle;
use App\Http\Requests\article\UpdateArticle;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ArticleController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::all();
        return view('article.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('article.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreArticle $request)
    {
        $fileNameWithExt = $request->file('image')->getClientOriginalName();
        $filExt = pathinfo($fileNameWithExt, PATHINFO_EXTENSION);
        $rename = uniqid() . '.' . $filExt;

        $article = new Article();
        $article ->fill($request->all());
        $article ->image = $rename;
        $article ->user_id = Auth::user()->id;
        $article ->save();

        $request->file('image')->storeAs('public/article_image', $rename);

        return redirect()->route('article.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::find($id);
        return view('article.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);

        return view('article.edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateArticle $request, $id)
    {

        $article = Article::find($id);
        $article->title = $request->title;
        $article->detail = $request->detail;

        if($request->hasFile('image')){
            $fileNameWithExt = $request->file('image')->getClientOriginalName();
            $fileExt = pathinfo($fileNameWithExt, PATHINFO_EXTENSION);
            $rename = uniqid() . '.' . $fileExt;
            
            Storage::delete(['public/article_image/'. $article->image]);
            $request->file('image')->storeAs('public/article_image', $rename);

            $article->image = $rename;
        }

        $article->save();

        return redirect()->route('profile.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
