<?php

return [
    'required' => 'กรุณากรอกข้อมูล :attribute',
    'max' => [
        'numeric' => ' :attribute ต้องไม่มากกว่า :max',
        'file' => ' :attribute ต้องไม่มากกว่า :max KB',
        'string' => ' :attribute ต้องไม่มากกว่า :max ตัวอัการ',
        'array' => ' :attribute ต้องไม่เกิน :max รายการ',
    ],
    'unique' => ' :attribute ถูกใช้งานแล้ว',

    'attributes' => [
        'first_name' => 'ชื่อ',
        'last_name' => 'นามสกุล',
        'username' => 'Username',
        'title' => 'หัวข้อ',
        'image' => 'รูปภาพ',
        'detail' => 'รายละเอียด',
    ]
];
