@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                 <h4> Edit Article</h4>
                 <form action="{{ route('article.update', ['article' => $article->id]) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label >Title</label>
                        <input type="text" value="{{ $article->title }}" id="title" name="title" maxlength="100" class="form-control @error('title') is-invalid @enderror" required>

                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="image">Images <small>{{ $article->image }}</small></label> 
                        <input type="file" id="image" name="image" accept="image/*" class="form-control @error('image') is-invalid @enderror" >

                        @error('image')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="detail">Detail</label>
                        <textarea id="detail" name="detail" cols="30" role="10" class="form-control @error('detail') is-invalid @enderror" required>{{ $article->detail }}</textarea>

                        @error('detail')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-right">
                        <input type="submit" class="btn btn-primary" value="Save">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
