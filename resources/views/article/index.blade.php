@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @foreach($articles as $article)
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-img-top">
                            <img src="{{ url('storage/article_image/' . $article->image) }}" class="img-fluid" alt="{{ $article->title }}">
                        </div>
                        <div class="card-body">
                            <h4>{{  $article->title }}</h4>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
