@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="crad">
            <div class="crad-body text-center">
                <h2>{{  $article->title }}</h2>
                <img src="{{ url('storage/article_image/' . $article->image) }}" alt="{{ $article->image }}">
            </div>
            <div class="mt-4 article-detail">{{ $article->detail }}</div>
        </div>
    </div>
@endsection
