@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        <p>ชื่อ: {{ $user->first_name }}</p>
                        <p>นามสกุล: {{ $user->last_name }}</p>
                        <p>username: {{ $user->username }}</p>
                        <div class="text-center">
                            <a href="{{ route('profile.edit', (['profile' => $user->id])) }}" class="btn btn-primary">
                                แก้ไข
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
               <div class="row">
                   @foreach ($articles as $article)
                   <div class="col-md-3">
                      <a href="{{ route('article.show', ['article' => $article->id]) }}">
                        <div class="card">
                            <div class="card-img-top">
                                <img src="{{ url('storage/article_image/' .  $article->image)}}" class="crad-img-cover" alt="{{ $article->image }}">
                            </div>
                            <div class="card-body">
                                 {{ $article->title }}
                                 <div class="text-right">
                                     <a href="{{ route('article.edit', ['article' => $article->id]) }}" class="btn btn-outline-primary">แก้ไข</a>
                                 </div>
                            </div>
                        </div>
                      </a>
                   </div>
                   @endforeach
               </div>
            </div>
        </div>
    </div>
@endsection
